import img3 from "./section1_svg/chiziq1.svg";
import React from "react";

function Page2(props) {
    return (
        <div>
            <div className={"ikki"}>
                <img className={"chiziq"} src={img3} alt="#"/>
                <p className={"ikki_p"}>Oʻz kasbining haqiqiy mutaxassislari <br/>orqali tashkillashtirilgan. Biz uchun
                    har <br/> bir oʻquvchimizning alohida qadri va <br/> oʻrni bor. Bizning shior esa Bir
                    maqsad <br/> yoʻlida, birgalikda harakat qilish!</p>
            </div>
        </div>
    );
}

export default Page2;