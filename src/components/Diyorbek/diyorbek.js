import React from 'react';
import "./main.css"
import Page1 from "./page1";
import Page2 from "./page2";
import Page3 from "./page3";


function Diyorbek(props) {
    return (
        <div>
            <div className="container">
                {/*<Carousel/>*/}
                <Page1/>
                <Page2/>
                <Page3/>
            </div>
        </div>
    );
}

export default Diyorbek;

