import img1 from "./section1_svg/img.svg";
import img2 from "./section1_svg/opa (2).png";
import React from "react";


function Page1(props) {
    return (
        <div>
            <div>
                <div className="tepa">
                    <div>
                        <div className="d-flex">
                            <img src={img1} alt="#"/>
                            <h2 className="h2_left">
                                Koreya davlati bo’yicha <br/>
                                TOP universitetlarda<br/>
                                tahsil olish imkoni
                            </h2>
                        </div>
                        <div className="d-flex">
                            <img src={img1} alt="#"/>
                            <h2 className="h2_left">Intensiv kurslar orqali<br/>
                                xorijiy tillarni, tez va<br/>
                                sifatli oʻrganish</h2>
                        </div>
                        <div className="d-flex">
                            <img src={img1} alt="#"/>
                            <h2 className="h2_left">Farzandlar kelajagi<br/>
                                uchun, harakatni<br/>
                                bugundan boshlash</h2>
                        </div>
                    </div>
                    <img className="img" src={img2} alt="#"/>
                </div>
            </div>
            <h2 className={"English"}>English Campus oʻquv markazi, oʻzidagi bir-necha <br/> yillik tajribasi va
                1000 dan ortiq
                kelajakdagi yoʻlini <br/> qidirayotgan
                yosh yigit-qizlarga oʻz yordamini <br/> berishga ulgurdi.</h2>
        </div>
    );
}

export default Page1;