import uch1 from "./section1_svg/uch 1.png";
import uch2 from "./section1_svg/uch 2.png"
import uch3 from "./section1_svg/uch 3.png"
import uch4 from "./section1_svg/uch 4.png"
import sana from "./section1_svg/sana.svg"
import qumsoat from "./section1_svg/qumsoat.svg"
import strelka from "./section1_svg/strelka (3).svg"
import qongiroq from "./section1_svg/qong'iroq.svg"
import ptichka from "./section1_svg/ptichka.svg"
import katnay from "./section1_svg/karnay.svg"
import dumaloq from "./section1_svg/dumaloq.svg"
import idumaloq from "./section1_svg/idumaloq.svg"
import togri from "./section1_svg/togri.svg"
import idum from "./section1_svg/idum.svg"
import i2 from "./section1_svg/i2.svg"
import belgi from "./section1_svg/belgi.svg"

import React from "react";

function Page3(props) {
    return (
        <div>


            <div className="grid gap-4 grid-cols-2 grid-rows-2 bg mt-[80px] ">

                <div className="rounded-[20px] bg-[#FAFAFA]">
                    <div className="flex ">
                        <div className="m-10">
                            <h2 className="font-abold">Ingliz tili kursi</h2>
                            <p>Tanishish menyusi</p>
                        </div>
                        <img className="ml-[56px] w-27 h-40 mt-10" src={uch1} alt="#"/>
                    </div>

                    <div className="mt-[54px] ml-10">
                        <div className="flex">
                            <img src={sana} alt="#"/>
                            <p className="ml-[10px] mt-[19px]">Dars kunlari:</p>
                            <h6 className="ml-[155px] mt-[21px]">Haftada 3 marotaba</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-4" src={qumsoat} alt="#"/>
                            <p className="ml-[10px]">Dars davomiyligi:</p>
                            <h6 className="mt-1 ml-[121px]">90 minut</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-[14px]" src={strelka} alt="#"/>
                            <p className="ml-[10px]">Intensiv kurs:</p>
                            <h6 className="mt-[3px] ml-[150px]">2 oy</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-4" src={qongiroq} alt="#"/>
                            <p className="ml-[10px]">Har bir bosqich davomiyligi:</p>
                            <h6 className="mt-[3px] ml-11">3 oy</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-4" src={ptichka} alt="#"/>
                            <p className="ml-[10px]">IELTS tayyorlov kursi:</p>
                            <h6 className="mt-[3px] ml-[95px]">Boshlang'ich tayyorlov 2 oy</h6>
                        </div>

                        <button className="flex mb-10 bg-white w-[556px] -ml-[0px] m-10 rounded-[30px]">
                            <p className="mt-[19px] text-center ml-[226px]">Kursga yozilish</p>
                            <img className="p-[14px]" src={belgi} alt="#"/>
                        </button>

                    </div>
                </div>

                <div className="rounded-[20px] bg-[#FAFAFA]">
                    <div className="flex ">
                        <div className="m-10">
                            <h2>Koreys tili kursi</h2>
                            <p>Tanishish menyusi</p>
                        </div>
                        <img className="ml-[29px] w-27 h-40 mt-10" src={uch2} alt="#"/>
                    </div>
                    <div className="mt-[54px] ml-10">
                        <div className="flex">
                            <img src={sana} alt="#"/>
                            <p className="ml-[10px] mt-[19px]">Dars kunlari:</p>
                            <h6 className="ml-[155px] mt-[21px]">Haftada 3 marotaba</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-4" src={qumsoat} alt="#"/>
                            <p className="ml-[10px]">Dars davomiyligi:</p>
                            <h6 className="mt-1 ml-[121px]">90 minut</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-[14px]" src={dumaloq} alt="#"/>
                            <p className="ml-[10px]">1 oylik darslar soni:</p>
                            <h6 className="mt-[3px] ml-[106px]">12 ta</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-4" src={qongiroq} alt="#"/>
                            <p className="ml-[10px]">Har bir bosqich davomiyligi:</p>
                            <h6 className="mt-[3px] ml-11">3 oy</h6>
                        </div>
                        <button className="flex bg-white w-[556px] -ml-[0px] m-10 rounded-[30px]">
                            <p className="mt-[19px] text-center ml-[226px]">Kursga yozilish</p>
                            <img className="p-[14px]" src={belgi} alt="#"/>
                        </button>
                    </div>
                </div>

                <div className="rounded-[20px] bg-[#FAFAFA]">
                    <div className="flex ">
                        <div className="m-10">
                            <h2>Rus tili kursi</h2>
                            <p>Tanishish menyusi</p>
                        </div>
                        <img className="ml-[73px] w-27 h-40 mt-10" src={uch3} alt="#"/>
                    </div>

                    <div className="mt-[54px] ml-10">
                        <div className="flex">
                            <img src={sana} alt="#"/>
                            <p className="ml-[10px] mt-[19px]">Dars kunlari:</p>
                            <h6 className="ml-[155px] mt-[21px]">Haftada 3 marotaba</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-4" src={qumsoat} alt="#"/>
                            <p className="ml-[10px]">Dars davomiyligi:</p>
                            <h6 className="mt-1 ml-[121px]">90 minut</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-[14px]" src={strelka} alt="#"/>
                            <p className="ml-[10px]">Intensiv kurs:</p>
                            <h6 className="mt-[3px] ml-[150px]">2 oy</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-4" src={qongiroq} alt="#"/>
                            <p className="ml-[10px]">Har bir bosqich davomiyligi:</p>
                            <h6 className="mt-[3px] ml-11">3 oy</h6>
                        </div>

                        <div className="flex">
                            <img className="-mt-4" src={katnay} alt="#"/>
                            <p className="ml-[10px]">Mustaqil so'zlashuv:</p>
                            <h6 className="mt-[3px] ml-[101px]">3 oy</h6>
                        </div>
                        <button className="flex mb-10 bg-white w-[556px] -ml-[0px] m-10 rounded-[30px]">
                            <p className="mt-[19px] text-center ml-[226px]">Kursga yozilish</p>
                            <img className="p-[14px]" src={belgi} alt="#"/>
                        </button>
                    </div>
                </div>

                <div className="rounded-[20px] bg-[#FAFAFA]">
                    <div className="flex ">
                        <div className="m-10">
                            <h2>Nega aynan <br/>
                                Biz?</h2>
                        </div>
                        <img className="ml-[76px] w-27 h-40 mt-10" src={uch4} alt="#"/>
                    </div>
                    <p className="ml-10 mt-14">English Campus oʻquv markazi oʻquvchilarini shunchaki kelib oʻqib
                    {/*    ketishlarini emas, balki natijaga erishishlarining tarafdoridir! Biz sizga natijaga*/}
                        erishishingizni
                        kafolatlaymiz.</p>
                    <button className="flex mb-10 bg-white w-[556px] ml-10 mt-[185px] m-10 rounded-[30px]">
                        <p className="mt-[19px] text-center ml-[226px]">Kursga yozilish</p>
                        <img className="p-[14px]" src={belgi} alt="#"/>
                    </button>
                </div>
            </div>

            <div>
                <div className="grid gap-4 grid-cols-2 grid-rows-2 bg mt-[80px]">
                    <div className="rounded-[20px] bg-[#FAFAFA]">
                        <div className="flex ">
                            <div className="m-10">
                                <h2>Mavjud <br/>
                                    mumammolar</h2>
                            </div>
                            <img className=" w-27 h-40 mt-10 h-[60px] ml-[231px] mt-[50px]" src={idumaloq} alt="#"/>
                        </div>

                        <div className="mt-[54px] ml-10">
                            <div className="flex">
                                <img src={idum} alt="#"/>
                                <p className="ml-[10px] mt-[19px]">Til o’rganishdagi dangasalikni yenga olmaslik</p>
                            </div>

                            <div className="flex">
                                <img className="-mt-4" src={idum} alt="#"/>
                                <p className="ml-[10px]">Sifatsiz ta’lim beruvchi ustozlar</p>
                            </div>

                            <div className="flex">
                                <img className="-mt-[14px]" src={idum} alt="#"/>
                                <p className="ml-[10px]">Zerikarli darslar</p>
                            </div>

                            <div className="flex">
                                <img className="-mt-4" src={idum} alt="#"/>
                                <p className="ml-[10px]">Noaniq yo’nalishda dars berilishi</p>
                            </div>

                            <div className="flex">
                                <img className="-mt-4" src={idum} alt="#"/>
                                <p className="ml-[10px]">Xorijiy tilni o’rganib bo’lish uchun, aniq vaqt qo’yilmasligi</p>
                            </div>
                        </div>
                    </div>
                    <div className="rounded-[20px] bg-[#FAFAFA]">
                        <div className="flex ">
                            <div className="m-10">
                                <h2>Muammolarning <br/>
                                    bizdagi yechimi</h2>
                            </div>
                            <img className=" w-27 h-40 mt-10 h-[60px] ml-[194px] mt-[50px]" src={togri} alt="#"/>
                        </div>

                        <div className="mt-[54px] ml-10">
                            <div className="flex">
                                <img src={i2} alt="#"/>
                                <p className="ml-[10px] mt-[19px]">Xorijiy tilni o’rganish davridagi o’zini qo’lga olish va dangasalikni yengish bo’yicha ustozlardan maslahat</p>
                            </div>

                            <div className="flex">
                                <img className="-mt-4" src={i2} alt="#"/>
                                <p className="ml-[10px]">Eng malakali  O’zbekiston bo’ylab ko’zga ko’ringan mutaxassislardan bilim olish imkoni</p>
                            </div>

                            <div className="flex">
                                <img className="-mt-[14px]" src={i2} alt="#"/>
                                <p className="ml-[10px]">Aniq metodikalar asosida Intensiv darsliklar
                                    ketma-ketligi</p>
                            </div>

                            <div className="flex mb-[66px]">
                                <img className="-mt-4" src={i2} alt="#"/>
                                <p className="ml-[10px]">Belgilangan muddat ichida xorijiy tilni oson o’rganish</p>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Page3;