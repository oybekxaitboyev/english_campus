import React from "react";
import cat from "../../assets/images_gif/cat.gif"
import boy from "../../assets/images_gif/smart-boy.gif"
import cofe from "../../assets/images_gif/cofe.gif"
import olmaxon from '../../assets/images_gif/olmaxon.gif'
import yer from '../../assets/🌏.svg'
import kitob from '../../assets/📚.svg'
import bola from '../../assets/👶.svg'
import svgg from '../../assets/Group (1).svg'
import svg from '../../assets/Group (9).svg'
import strelka from '../../assets/Group (5).svg'
import strelkaa from '../../assets/Group (6).svg'


export const Home = () => {
  return (
    <div className="container-fuild">
      <h1 className="bizdan">Bizdan   <img className="olmaxon" src={olmaxon}/> Siz uchun<img className="svgg" src={svgg}/></h1>
<div className="til">
<h1 className="oqish"><img src={yer}/>Xorijda o`qish</h1>
      <span><img className="cofe" src={cofe}/></span>
       <h1 className="oqish"><img src={kitob}/>Til kurslari</h1>
</div>
<div className="kurs">
<img className="cat" src={cat}/><h1 className="oqish"> <img src={bola}/>Bolalar kurslari
       </h1>   <img className="boy" src={boy}/>
</div>
<div className="svgs">
<img className="strelka" src={strelkaa}/>
  <img className="strelkaa" src={strelka}/>
</div>
    </div>
  );
};