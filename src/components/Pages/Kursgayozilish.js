import React, {useState} from "react";
import img from "../../assets/image 7.png"
import './Modal.css'

export const Kursgayozilish =() =>{
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);

  if(click) {
    document.body.classList.add('active-modal')
  } else {
    document.body.classList.remove('active-modal')
  }

  return (
    <>
    <button onClick={handleClick} className="btn-modal">
        Open
      </button>
        

      {click && (
        <div className="modal">
          <div onClick={handleClick} className="overlay"></div>
          <div className="modal-content">
            <h2>Hello Modal</h2>
            <img src={img} />
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident
              perferendis suscipit officia recusandae, eveniet quaerat assumenda
              id fugit, dignissimos maxime non natus placeat illo iusto!
              Sapiente dolorum id maiores dolores? Illum pariatur possimus
              quaerat ipsum quos molestiae rem aspernatur dicta tenetur. Sunt
              placeat tempora vitae enim incidunt porro fuga ea.
            </p>
            <button className="close-modal" onClick={handleClick}>
            <i class="fa fa-times" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      )}
    </>
  );
}