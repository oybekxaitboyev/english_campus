/* eslint-disable react/jsx-no-target-blank */
import React, { useEffect, useState } from 'react';
import App from "../App"
import { FaYoutube, FaFacebook, FaInstagram, FaTelegram } from "react-icons/fa";
import AOS from "aos";
import "aos/dist/aos.css";


function ContactMe() {
  useEffect(() => {
    AOS.init({});
  });
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [text, setText] = useState("");
  let url = `https://api.telegram.org/bot6004747091:AAEPHQcyYt_zvc2OBkSJ6UqBCiTCEhk7ZQQ/sendMessage?chat_id=1197709139&text=%0A-Isim: ${name}, %0A- Raqam: ${phone},  %0A- Matin: ${text}, %0A`;
  function sendMassageFunction() {
    fetch(url).then((res) => res.json());
  }
  return (
    <div className={"contact"} data-aos="flip-down" data-aos-duration="2000">
      <div className="tag">
        <h1>Aloqaga</h1>
        <div className='tag2'>
       
        <h1>chiqish</h1>
        </div>
      
      </div>
  <div className='umumiy'>
 <form>
        <input
          defaultValue={name}
          onChange={(e) => setName(e.target.value)}
          id="name"
          type="text"
          placeholder={"Ismingiz"}
        />

        <input
          defaultValue={phone}
          onChange={(e) => setPhone(e.target.value)}
          id={"phone"}
          type="tel"
          placeholder={"Telefon raqamingiz"}
        />

        <textarea
          defaultValue={text}
          onChange={(e) => setText(e.target.value)}
          id={"text"}
          placeholder={"Xabaringizni shu yerga yozing..."}
        />
        <button onClick={sendMassageFunction} type="submit">
          Xabarni yuborish
        </button>
      </form>
  </div>
     
      <div className="footer">
        <a href="https://www.facebook.com/home.php" target="_blank">
          <FaFacebook className="icon" />
        </a>
        <a href="https://www.instagram.com/" target="_blank">
          <FaInstagram className="icon" />
        </a>
        <a href="https://t.me/english_campus_bot" target="_blank">
          <FaTelegram className="icon" />
        </a>
        <a href="https://www.youtube.com/" target="_blank">
          <FaYoutube className="icon" />
        </a>

        <p>© 2022 English Campus. All rights reserved.</p>
      </div>
    </div>
  );
}

export default ContactMe;