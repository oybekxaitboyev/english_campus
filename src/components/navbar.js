import React, {useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import "./navbar.css";
import './Pages/Modal.css'
import campus from '../assets/Group 109 (2).svg'
import icon from '../assets/Group 1.svg'
import yer from '../assets/🌏.svg'
import kitob from '../assets/📚.svg'
import bola from '../assets/👶.svg'
import gb from '../assets/🇬🇧.svg'
import kr from '../assets/🇰🇷.svg'
import ru from '../assets/🇷🇺.svg'
import AOS from "aos";
import "aos/dist/aos.css";
import "./Button.css";





function NavBar() {
  useEffect(() => {
    AOS.init({});
  });
  
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);

  const [modal, setModal] = useState(false);
  const toggleModal = () => setModal(!click);

  const [formData, setFormData] = useState({name: "",email: "",message: ""});

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    alert(`Name: ${formData.name}, Email: ${formData.email}, Message: ${formData.message}`
    );
  }
  
  if(modal) {
    document.body.classList.add('active-modal')
  } else {
    document.body.classList.remove('active-modal')
  }
  return (
    <>
      <nav className="navbar" data-aos="fade-up" data-aos-duration="3000">
        <div className="nav-container">
          <img src="testSvg" alt="" />
          <NavLink
            exact
            to="/"
            activeClassName="active"
            className="nav-logo"
            onClick={handleClick}
          >
            <img src={campus} />
          </NavLink>

          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item">
              <NavLink
                exact
                to="bizhaqimizda"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Biz haqimizda
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/tilkurslari"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Til kurslari
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/xorijdaoqish"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Xorijda oqish
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/bolalarkursi"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Bolalar kurslari
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/aloqa"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
              >
                Aloqa
              </NavLink>
            </li>
            <li className="nav-items">
              <NavLink
                exact
                to="kursgayozilish"
                activeClassName="active"
                className="nav-linkss"
                onClick={toggleModal}
              >
                Kursga yozilish <img src={icon} />
              </NavLink>
            </li>
          </ul>

          {modal && (
            <div className="modal">
              <div onClick={toggleModal} className="overlay"></div>
              <div className="modal-content">
                <h1 className="ariza">Kurs uchun ariza yuborish</h1>
                <p className="bolim">Bo`limni tanlang</p>
                <button className="btnn">
                  <img src={kitob} />
                  Til kurslari
                </button>
                <button className="btnn">
                  <img src={yer} />
                  Xorijda oqish
                </button>
                <button className="btnn">
                  <img src={bola} />
                  Bolalar kurslari
                </button>
                <p className="bolim">Ichki bolimni tanlang</p>
                <button className="btnn">
                  <img src={gb} />
                  Ingliz tili
                </button>
                <button className="btnn">
                  <img src={kr} />
                  Koreys tili
                </button>
                <button className="btnn">
                  <img src={ru} />
                  Rus tilii
                </button>
                <p className="bolim">O`zingiz haqizngizda </p>

                <form onSubmit={handleSubmit}>
                  <div className="inputs">
                    <label htmlFor="name"></label>
                    <input
                      className="input"
                      type="text"
                      id="name"
                      name="name"
                      placeholder="Ismingiz"
                      value={formData.name}
                      onChange={handleChange}
                    />

                    <label htmlFor="email"></label>
                    <input
                      className="input"
                      type="email"
                      id="email"
                      name="email"
                      placeholder="Telefon raqamingiz"
                      value={formData.email}
                      onChange={handleChange}
                    />
                    <br />
                  </div>

                  <label htmlFor="message"></label>
                  <textarea
                    className="input-t"
                    id="message"
                    name="message"
                    placeholder="Qanday savollaringiz bor, bu yerga yozing..."
                    value={formData.message}
                    onChange={handleChange}
                  />
                  <br />

                  <button className="btn" type="submit">
                    Kurs uchun arizani yuborish
                  </button>
                </form>
                <button className="close-modal" onClick={toggleModal}>
                  <i class="fa fa-times" aria-hidden="true"></i>
                </button>
              </div>
            </div>
          )}
          <div className="nav-icon" onClick={handleClick}>
            <i className={click ? "fas fa-times" : "fas fa-bars"}></i>
          </div>
        </div>
      </nav>
      <button className="Button">Click me</button>
    </>
  );
  
}

export default NavBar;