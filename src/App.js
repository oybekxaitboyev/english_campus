import  React from "react"
import './App.css';
import Contact from './components/contact';
import Navbar from "./components/navbar";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Home } from "./components/Pages/Home";
import { Bizhaqimizda } from "./components/Pages/Bizhaqimizda";
import { Tilkurslari } from "./components/Pages/Tilkurslari";
import { Xorijdaoqish } from "./components/Pages/Xorijdaoqish";
import { Bolalarkursi } from "./components/Pages/Bolalarkursi";
import { Aloqa } from "./components/Pages/Aloqa";
import { Kursgayozilish } from "./components/Pages/Kursgayozilish";

import Diyorbek from "./components/Diyorbek/diyorbek"

export default function App() {
  return (
    <>
      <Router>
        <Navbar />
        <div className="pages">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/bizhaqimizda" element={<Bizhaqimizda />} />
            <Route path="/tilkurslari" element={<Tilkurslari />} />
            <Route path="/xorijdaoqish" element={<Xorijdaoqish />} />
            <Route path="/bolalarkursi" element={<Bolalarkursi />} />
            <Route path="/aloqa" element={<Aloqa />} />
            <Route path="/kursgayozilish" element={<Kursgayozilish />} />
          </Routes>
        </div>
      </Router>
      <Contact />
      <Diyorbek/>
    </>
  );
}













